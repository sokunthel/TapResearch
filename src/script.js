(function() {
  'use strict';
  window.addEventListener('load', function() {
    var form = document.getElementById('trForm');
    form.addEventListener('submit', function(event) {
      event.preventDefault();
      event.stopPropagation();
      if (form.checkValidity()) {
        let params = "api_token=9a7fb35fb5e0daa7dadfaccd41bb7ad1&user_identifier=" + document.getElementById('userId').value;
        let elScript = document.createElement('script');
        elScript.setAttribute('src', 'https://www.tapresearch.com/supply_api/surveys/offer?callback=fnRetrieveData&' + params);
        document.body.appendChild(elScript);

        // After the script is loaded (and executed), remove it
        elScript.onload = function () {
          this.remove();
        };
      }
      form.classList.add('was-validated');
    }, false);
  }, false);

  window.fnRetrieveData = function(data) {
    let newDiv = document.createElement('div');
    if (data && data.has_offer) {
      newDiv.innerHTML = "<p class='typo-light'>Thanks for participating! Please click the button below to start the survey.</p><a href='" + data.offer_url + "' target='_blank'><button type='button' class='btn btn-outline-info'>Take Survey</button></a><div class='tr-divider'></div><h3 class='typo-light'>Rewards</h3><ul class='tr-minmax'><li>Min<div class='tr-circle'>" + data.message_hash.min + "<div class='tr-currency'>" + data.message_hash.currency + "</div></div></li><li>Max<div class='tr-circle'>" + data.message_hash.max + "<div class='tr-currency'>" + data.message_hash.currency + "</div></div></li></ul>";
    } else {
      newDiv.innerHTML = "No survey available";
    }
    let wrapper = document.querySelector('.tr-wrapper');
    wrapper.classList.remove('active');
    wrapper.innerHTML = '';
    wrapper.appendChild(newDiv);
    setTimeout(function() {
      wrapper.classList.add('active');
    }, 30);
    // console.log(data);
  };
})();
