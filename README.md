Requirements
1. Create a view with a text field that takes in an alphanumeric string (user identifier) and a submit button.
2. On submit, make a request to our “Get Survey Offer” API endpoint.
  a. http://supply-docs.tapresearch.com/#get-survey-offer
      This endpoint accepts a “callback” parameter for usage in a JSONP request, to address cross domain issues
  b. API Token: 9a7fb35fb5e0daa7dadfaccd41bb7ad1
  c. Ignore the device identifier parameter
  d. User identifier: “tapresearch”
    i. Anything else will yield has_offer: false
3. Handle response based on the has_offer attribute.
  a. has_offer: true
    i. Add a “Take Survey” link that points to the offer URL.
    ii. Show the min/max reward amount along with the currency name.
  b. has_offer: false
    i. Display “No survey available”.

Bonus
1. Open a new tab when a user clicks on the “Take Survey” link.
2. Handle timeout scenario.
3. Validations
  a. Non-empty user identifier
  b. User identifier cannot exceed 32 characters in length

--------

Tips:
* On Mac run `php -S localhost:8000` in the project folder for local testing